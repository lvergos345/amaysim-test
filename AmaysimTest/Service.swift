import Foundation

struct Service {
    let msn: String
    let credit: Double
    let creditExpiry: Date
    let dataUsageThreshold: Bool
    let subscription: Subscription

    init?(jsonServiceDictionary: Dictionary<String, AnyObject>,
          jsonSubscriptionDictionary: Dictionary<String, AnyObject>,
          jsonProductDictionary: Dictionary<String, AnyObject>) {
        print(jsonServiceDictionary)
        guard let msn = jsonServiceDictionary["msn"] as? String,
            let credit = jsonServiceDictionary["credit"] as? Double,
            let creditExpiryDateString = jsonServiceDictionary["credit-expiry"] as? String,
            let creditExpiry = Date.dateFromString(creditExpiryDateString),
            let dataUsageThreshold = jsonServiceDictionary["data-usage-threshold"] as? Bool,
            let subscription = Subscription(jsonSubscriptionDictionary:jsonSubscriptionDictionary, jsonProductDictionary:jsonProductDictionary)
            else {
                print("failed to load Service from json: \(jsonServiceDictionary)")
                return nil
        }
        self.msn = msn
        self.credit = credit
        self.creditExpiry = creditExpiry
        self.dataUsageThreshold = dataUsageThreshold
        self.subscription = subscription
    }
}
