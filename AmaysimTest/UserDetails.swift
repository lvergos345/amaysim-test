import Foundation

struct UserDetails {
    let title: String
    let firstName: String
    let lastName: String
    let birthDate: Date
    let contactNumber: String
    let emailAddress: String
    let emailAddressVerified: Bool
    
    var fullNameWithTitle: String {
        return "\(title) \(firstName) \(lastName)"
    }
    
    init?(jsonAccountDictionary: Dictionary<String, AnyObject>) {
        guard let title = jsonAccountDictionary["title"] as? String,
            let firstName = jsonAccountDictionary["first-name"] as? String,
            let lastName = jsonAccountDictionary["last-name"] as? String,
            let birthDateString = jsonAccountDictionary["date-of-birth"] as? String,
            let birthDate = Date.dateFromString(birthDateString),
            let contactNumber = jsonAccountDictionary["contact-number"] as? String,
            let emailAddress = jsonAccountDictionary["email-address"] as? String,
            let emailAddressVerified = jsonAccountDictionary["email-address-verified"] as? Bool
        else {
                return nil
        }
        self.title = title
        self.firstName = firstName
        self.lastName = lastName
        self.birthDate = birthDate
        self.contactNumber = contactNumber
        self.emailAddress = emailAddress
        self.emailAddressVerified = emailAddressVerified
    }
}
