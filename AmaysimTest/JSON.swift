import Foundation

struct JSON {
    
    // MARK: JSON File Loading
    
    static func loadJSONDictionaryFromFileName(_ fileName: String, type: String = "json") -> Dictionary<String, AnyObject>? {
        guard let filePath = Bundle.main.path(forResource: fileName, ofType: type),
            let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) else {
                return nil
        }
        
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            if let jsonDictionary = jsonObject as? [String: AnyObject] {
                return jsonDictionary
            }
        } catch(let error) {
            print(error)
            return nil
        }
        return nil
    }
    
    static func parseNull(_ value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
}
