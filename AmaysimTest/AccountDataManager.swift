import Foundation

public struct AccountDataManager {
    var account: Account
    
    init?() {
        self.init(jsonFilePath:"collection")
    }
    
    init?(jsonFilePath: String) {
        guard let jsonDictionary = JSON.loadJSONDictionaryFromFileName(jsonFilePath),
            let jsonDataDictionary = jsonDictionary["data"] as? [String: AnyObject],
            let jsonAccountAttributesDictionary = jsonDataDictionary["attributes"] as? [String: AnyObject],
            let jsonIncludedArray = jsonDictionary["included"] as? [[String: AnyObject]],
            let jsonServiceAttributesDictionary = jsonIncludedArray[0]["attributes"] as? [String: AnyObject],
            let jsonSubscriptionAttributesDictionary = jsonIncludedArray[1]["attributes"] as? [String: AnyObject],
            let jsonProductAttributesDictionary = jsonIncludedArray[2]["attributes"] as? [String: AnyObject],
            let service = Service(jsonServiceDictionary:jsonServiceAttributesDictionary,
                                  jsonSubscriptionDictionary: jsonSubscriptionAttributesDictionary,
                                  jsonProductDictionary: jsonProductAttributesDictionary),
            let account = Account(jsonAccountDictionary: jsonAccountAttributesDictionary, service: service)
        else {
            return nil
        }
        
        self.account = account
    }
}