//
//  InitialViewController.swift
//  AmaysimTest
//
//  Created by Luke Vergos on 16/02/2017.
//  Copyright © 2017 Luke Vergos. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    var accountDataManager = AccountDataManager()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !accountDataManager.account.isLoggedIn {
            self.performSegue(withIdentifier: "LoginModalSegue", sender: self)
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginModalSegue" {
            let loginViewController = segue.destination as! LoginViewController
            loginViewController.accountDataManager = accountDataManager
            loginViewController.loginSuccessfulcompletionBlock = { [weak self] (accountDataManager) in
                self?.accountDataManager = accountDataManager
                self?.dismiss(animated: true, completion: { 
                    self?.performSegue(withIdentifier: "WelcomeSegue", sender: self)
                })
            }
        } else if segue.identifier == "WelcomeSegue" {
            let welcomeViewController = segue.destination as! WelcomeViewController
            welcomeViewController.accountDataManager = accountDataManager
        }
    }
}
