import Foundation

struct Subscription {
    let includedDataBalanceInMB: Int
    let includedCreditBalance: Int?
    let includedInternationalTalkBalance: Int?
    let expiryDate: Date
    let autoRenewal: Bool
    let product: Product
    
    var includedDataBalanceInGB: Double {
        return Double(includedDataBalanceInMB) / 1000
    }
    
    init?(jsonSubscriptionDictionary: Dictionary<String, AnyObject>,
          jsonProductDictionary: Dictionary<String, AnyObject>) {
        guard let includedDataBalanceInMB = jsonSubscriptionDictionary["included-data-balance"] as? Int,
            let expiryDateString = jsonSubscriptionDictionary["expiry-date"] as? String,
            let expiryDate = Date.dateFromString(expiryDateString),
            let autoRenewal = jsonSubscriptionDictionary["auto-renewal"] as? Bool,
            let product = Product(jsonProductDictionary: jsonProductDictionary)
            else {
                print("failed to load Subscription from json: \(jsonSubscriptionDictionary)")
                return nil
        }
        self.includedDataBalanceInMB = includedDataBalanceInMB
        self.includedCreditBalance = JSON.parseNull(jsonSubscriptionDictionary["included-credit-balance"]) as? Int
        self.includedInternationalTalkBalance = JSON.parseNull(jsonSubscriptionDictionary["included-international-talk-balance"]) as? Int
        self.expiryDate = expiryDate
        self.autoRenewal = autoRenewal
        self.product = product
    }
}
