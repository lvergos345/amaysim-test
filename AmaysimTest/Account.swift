import Foundation

struct Account {
    let userDetails: UserDetails
    let paymentType: String
    let unbilledCharges: String?
    let nextBillingDate: Date?
    var service: Service
    var isLoggedIn: Bool {
        return loggedIn
    }
    
    private var loggedIn: Bool = false
    
    init?(jsonAccountDictionary: Dictionary<String, AnyObject>, service: Service) {
        guard let user = UserDetails(jsonAccountDictionary: jsonAccountDictionary),
            let paymentType = jsonAccountDictionary["payment-type"] as? String
             else {
                return nil
        }
        
        self.userDetails = user
        self.paymentType = paymentType
        unbilledCharges = JSON.parseNull(jsonAccountDictionary["unbilled-charges"]) as! String?
        if let billingDateString = JSON.parseNull(jsonAccountDictionary["next-billing-date"]) as! String? {
            nextBillingDate = Date.dateFromString(billingDateString)
        } else {
            nextBillingDate = nil
        }
        self.service = service
    }
    
    mutating func login(_ msn: String) -> Bool{
        loggedIn = (service.msn == msn)
        return loggedIn
    }
}
