//
//  WelcomeViewController.swift
//  AmaysimTest
//
//  Created by Luke Vergos on 14/02/2017.
//  Copyright © 2017 Luke Vergos. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    var accountDataManager: AccountDataManager!

    override func viewDidLoad() {
        super.viewDidLoad()

        perform(#selector(WelcomeViewController.transitionToAccount), with: self, afterDelay: 2.5)
    }
    
    func transitionToAccount() {
        performSegue(withIdentifier: "AccountNavigationControllerSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AccountNavigationControllerSegue" {
            let accountNavigationController = segue.destination as! UINavigationController
            let accountViewController = accountNavigationController.viewControllers.first as! AccountViewController
            accountViewController.accountViewModel = AccountViewModel(account: accountDataManager.account)
        }
    }
}
