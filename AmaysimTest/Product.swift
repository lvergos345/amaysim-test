import Foundation

struct Product {
    let name: String
    let unlimitedText: Bool
    let unlimitedTalk: Bool
    let unlimitedInternationalText: Bool
    let unlimitedInternationalTalk: Bool
    let priceInCents: Int
    
    init?(jsonProductDictionary: Dictionary<String, AnyObject>) {
        guard let name = jsonProductDictionary["name"] as? String,
            let unlimitedText = jsonProductDictionary["unlimited-text"] as? Bool,
            let unlimitedTalk = jsonProductDictionary["unlimited-talk"] as? Bool,
            let unlimitedInternationalText = jsonProductDictionary["unlimited-international-text"] as? Bool,
            let unlimitedInternationalTalk = jsonProductDictionary["unlimited-international-talk"] as? Bool,
            let priceInCents = jsonProductDictionary["price"] as? Int
            else {
                print("failed to load Product from json: \(jsonProductDictionary)")
                return nil
        }
        self.name = name
        self.unlimitedText = unlimitedText
        self.unlimitedTalk = unlimitedTalk
        self.unlimitedInternationalText = unlimitedInternationalText
        self.unlimitedInternationalTalk = unlimitedInternationalTalk
        self.priceInCents = priceInCents
    }
}
