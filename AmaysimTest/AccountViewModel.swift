//
//  AccountViewModel.swift
//  AmaysimTest
//
//  Created by Luke Vergos on 16/2/17.
//  Copyright © 2017 Luke Vergos. All rights reserved.
//

import Foundation

struct AccountViewModel {
    let account: Account
    
    var msnString: String {
        return account.service.msn
    }

    var dataBalanceString: String {
        let data = account.service.subscription.includedDataBalanceInGB
        return "\(data)GB"
    }

    var creditBalanceString: String {
        if let amountInCents = account.service.subscription.includedCreditBalance,
            let amountString = currencyStringFromAmountInCents(amountInCents) {
            return amountString
        } else {
            return "-"
        }
    }

    var internationalTextbalanceString: String {
        if let amountInCents = account.service.subscription.includedInternationalTalkBalance,
            let amountString = currencyStringFromAmountInCents(amountInCents) {
            return amountString
        } else {
            return "-"
        }
    }

    var expiryDateString: String {
        let expiryDate = account.service.subscription.expiryDate
        return expiryDate.stringFromDate() ?? ""
    }

    
    var planNameString: String {
        return account.service.subscription.product.name
    }

    var planPriceString: String {
        let amountInCents = account.service.subscription.product.priceInCents
        return currencyStringFromAmountInCents(amountInCents) ?? ""
    }

    var includedTalkString: String {
        let unlimitedTalk = account.service.subscription.product.unlimitedTalk
        return  unlimitedTalk ? "UNLIMITED" : "N/A"
    }

    var includedTextString: String {
        let unlimitedText = account.service.subscription.product.unlimitedText
        return unlimitedText ? "UNLIMITED" : "N/A"
    }

    
    var accountHolderString: String {
        return account.userDetails.fullNameWithTitle
    }

    var contactNumberString: String {
        return account.userDetails.contactNumber
    }

    var emailString: String {
        return account.userDetails.emailAddress
    }
    
    private func currencyStringFromAmountInCents(_ amountInCents: Int) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        let amount = Double(amountInCents) / 100.00
        return formatter.string(from: NSNumber(value:amount))
    }
}
