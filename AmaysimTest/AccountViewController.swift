//
//  AccountViewController.swift
//  AmaysimTest
//
//  Created by Luke Vergos on 15/02/2017.
//  Copyright © 2017 Luke Vergos. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {
    var accountViewModel: AccountViewModel!
    
    @IBOutlet weak var usageSectionTitleLabel: UILabel!
    @IBOutlet weak var dataBalanceLabel: UILabel!
    @IBOutlet weak var creditBalanceLabel: UILabel!
    @IBOutlet weak var internationalTextLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    
    @IBOutlet weak var planNameLabel: UILabel!
    @IBOutlet weak var planPriceLabel: UILabel!
    @IBOutlet weak var includedTalkLabel: UILabel!
    @IBOutlet weak var includedTextLabel: UILabel!
    
    @IBOutlet weak var accountHolderLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usageSectionTitleLabel.text = "Usage For \(accountViewModel.msnString)"
        dataBalanceLabel.text = accountViewModel.dataBalanceString
        creditBalanceLabel.text = accountViewModel.creditBalanceString
        internationalTextLabel.text = accountViewModel.internationalTextbalanceString
        expiryDateLabel.text = accountViewModel.expiryDateString
        
        planNameLabel.text = accountViewModel.planNameString
        planPriceLabel.text = accountViewModel.planPriceString
        includedTalkLabel.text = accountViewModel.includedTalkString
        includedTextLabel.text = accountViewModel.includedTextString
        
        accountHolderLabel.text = accountViewModel.accountHolderString
        contactNumberLabel.text = accountViewModel.contactNumberString
        emailLabel.text = accountViewModel.emailString
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
}
