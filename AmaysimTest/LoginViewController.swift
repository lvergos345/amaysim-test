//
//  LoginViewController.swift
//  AmaysimTest
//
//  Created by Luke Vergos on 14/02/2017.
//  Copyright © 2017 Luke Vergos. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var msnTextField: UITextField!
    var accountDataManager: AccountDataManager!
    var loginSuccessfulcompletionBlock: ((AccountDataManager) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showAlert() {
        let alertController = UIAlertController(title: "Failed to login", message: "Unrecognized Mobile Serial Number", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Actions

    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if let msnString = msnTextField.text {
            if accountDataManager.account.login(msnString)  {
                loginSuccessfulcompletionBlock?(accountDataManager)
            } else {
                showAlert()
            }
        } else {
            showAlert()
        }
    }
}
